package org.example.lockscreen;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.AlarmClock;
import android.view.View;
import android.view.WindowManager;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;


public class MainActivity extends Activity {

    private Button mainBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Set up our Lockscreen
        makeFullScreen();
        showWhenLocked();
        startService(new Intent(this, LockScreenService.class));

        mainBtn = (Button) findViewById(R.id.button);
        mainBtn.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                createAlarm("Example Alarm",12,45);
            }
        });
    }

    /**
     * A simple method that sets the screen to fullscreen.  It removes the Notifications bar,
     *   the Actionbar and the virtual keys (if they are on the phone)
     */
    public void makeFullScreen() {
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if(Build.VERSION.SDK_INT < 19) { //View.SYSTEM_UI_FLAG_IMMERSIVE is only on API 19+
            this.getWindow().getDecorView()
                    .setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        } else {
            this.getWindow().getDecorView()
                    .setSystemUiVisibility(View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE);
        }
    }

    public void showWhenLocked() {
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN |
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON,
                WindowManager.LayoutParams.FLAG_FULLSCREEN |
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);
    }

    private void createAlarm(String message, int hour, int minutes) {
        Intent alarmIntent = new Intent(AlarmClock.ACTION_SET_ALARM)
                .putExtra(AlarmClock.EXTRA_MESSAGE, message)
                .putExtra(AlarmClock.EXTRA_HOUR, hour)
                .putExtra(AlarmClock.EXTRA_MINUTES, minutes)
                .putExtra(AlarmClock.EXTRA_SKIP_UI, true);
        if (alarmIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(alarmIntent);
        }
    }


//    private void openAlert(View view) {
//        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
//
//        alertDialogBuilder.setTitle(this.getTitle()+ " decision");
//        alertDialogBuilder.setMessage("Confirm activity trigger");
//        // set positive button: Yes message
//        alertDialogBuilder.setPositiveButton("Yes",new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog,int id) {
//                // go to a new activity of the app
//                Intent positveActivity = new Intent(getApplicationContext(),
//                        PositiveActivity.class);
//                startActivity(positveActivity);
//            }
//        });
//        // set negative button: No message
//        alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog,int id) {
//                // cancel the alert box and put a Toast to the user
//                dialog.cancel();
//                Toast.makeText(getApplicationContext(), "Event canceled.",
//                        Toast.LENGTH_LONG).show();
//            }
//        });
//        // set neutral button: Exit the app message
//        alertDialogBuilder.setNeutralButton("Unlock",new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog,int id) {
//                // exit the app and go to the HOME
//                MainActivity.this.finish();
//            }
//        });
//
//        AlertDialog alertDialog = alertDialogBuilder.create();
//        // show alert
//        alertDialog.show();
//    }

    @Override
    public void onBackPressed() {
        return; //Do nothing!
    }

    public void unlockScreen(View view) {
        //Instead of using finish(), this totally destroys the process
        android.os.Process.killProcess(android.os.Process.myPid());
    }
}